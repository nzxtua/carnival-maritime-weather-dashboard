# Carnival Maritime - Weather App

NPM / Vite / React / Mantine / TypeScript / ESLint / Prettier / Storybook

- Uses free OpenWeatherMap API to show Today's Weather / Forecast.
- As well as autocomplete City name and get it's coordinates. 

![Weather App](screenshot.png "Weather App")

## Minimal required steps
- Rename `.env.example` to `.env`

- Install deps: `npm install`
- Run project: `npm run dev`
- Open page: `localhost:5173`

- Run tests: `npm run vitest`