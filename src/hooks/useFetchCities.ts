import { AxiosError } 						   from 'axios'
import { UseQueryOptions, useQuery } from '@tanstack/react-query'
import { apiClient }                 from '@/service/api'
import { wait }                      from '@/utils/helpers'

const CITIES_QUERY_KEY   = 'cities'
const CITIES_QUERY_LIMIT = 5

const OPENWEATHER_API_KEY    = import.meta.env.VITE_OPENWEATHER_API_KEY
const OPENWEATHER_API_GEOURL = import.meta.env.VITE_OPENWEATHER_API_GEOURL

const getCities = async <T>(
	query  : string,
	signal : AbortSignal
) => {
	await wait(1000)

	const { data } = await apiClient<T>({
		url    : `/${OPENWEATHER_API_GEOURL}`,
		params : {
			q     : query,
			appid : OPENWEATHER_API_KEY,
			limit : CITIES_QUERY_LIMIT,
		},
		signal
	})

	return data
}

const useFetchCities = <T, R>(
	query    : string,
	options? : Partial<UseQueryOptions<T, AxiosError, R>>
) => useQuery({
	queryKey    : [CITIES_QUERY_KEY, query],
	queryFn     : ({ signal }) => getCities(query, signal),
	enabled     : !!query,
	initialData : [] as T,
	...options,
})

export { useFetchCities }
