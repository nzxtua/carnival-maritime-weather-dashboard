import { UseQueryOptions, useQuery } from '@tanstack/react-query'
import { apiClient }                 from '@/service/api'
import { wait }                      from '@/utils/helpers'

const CURRENT_WEATHER_QUERY_KEY  = 'current-weather'

const OPENWEATHER_API_KEY        = import.meta.env.VITE_OPENWEATHER_API_KEY
const OPENWEATHER_API_WEATHERURL = import.meta.env.VITE_OPENWEATHER_API_WEATHERURL

const getCurrentWeather = async <T>(
	coords : [string, string],
	signal : AbortSignal
) => {
	await wait(1000)

	const { data } = await apiClient<T>({
		url    : `/${OPENWEATHER_API_WEATHERURL}`,
		params : {
			lat   : coords[0],
			lon   : coords[1],
			units : 'metric',
			appid : OPENWEATHER_API_KEY,
		},
		signal
	})

	return data
}

const useFetchCurrentWeather = <T>(
	coords   : [string, string] | null,
	options? : Partial<UseQueryOptions<T>>
) => useQuery({
	queryKey : [CURRENT_WEATHER_QUERY_KEY, coords],
	queryFn  : ({ signal }) => getCurrentWeather<T>(coords!, signal),
	enabled  : !!coords?.[0] && !!coords?.[1],
	...options,
})

export { useFetchCurrentWeather }