import dayjs from 'dayjs'
import { useEffect, useState } from 'react'

export const useCurrentTime = (formatStr: string = 'HH:mm:ss') => {
	const [time, setTime] = useState(new Date())

	useEffect(() => {
		const interval = setInterval(() => {
			setTime(new Date())
		}, 1000)
		return () => clearInterval(interval)	
	}, [])

	return { time, formattedTime: dayjs(time).format(formatStr) }
}