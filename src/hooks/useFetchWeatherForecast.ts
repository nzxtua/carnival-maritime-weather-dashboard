import { UseQueryOptions, useQuery } from '@tanstack/react-query'
import { apiClient }                 from '@/service/api'
import { wait }                      from '@/utils/helpers'

const CURRENT_WEATHER_QUERY_KEY  = 'weather-forecast'

const OPENWEATHER_API_KEY         = import.meta.env.VITE_OPENWEATHER_API_KEY
const OPENWEATHER_API_FORECASTURL = import.meta.env.VITE_OPENWEATHER_API_FORECASTURL

const getWeatherForecast = async <T>(
	coords : [string, string],
	signal : AbortSignal
) => {
	await wait(1000)

	const { data } = await apiClient<T>({
		url    : `/${OPENWEATHER_API_FORECASTURL}`,
		params : {
			lat   : coords[0],
			lon   : coords[1],
			units : 'metric',
			appid : OPENWEATHER_API_KEY,
		},
		signal
	})

	return data
}

const useFetchWeatherForecast = <T>(
	coords   : [string, string] | null,
	options? : Partial<UseQueryOptions<T>>
) => useQuery({
	queryKey : [CURRENT_WEATHER_QUERY_KEY, coords],
	queryFn  : ({ signal }) => getWeatherForecast<T>(coords!, signal),
	enabled  : !!coords?.[0] && !!coords?.[1],
	...options,
})

export { useFetchWeatherForecast }