import { useSearchParams } from 'react-router-dom'
import { 
  Card, 
  Collapse, 
  Container, 
  Group, 
  LoadingOverlay, 
  Text, 
  Title 
}                          from '@mantine/core'
import { capitalize }      from '@/utils/helpers'
import { useCurrentTime }  from '@/hooks/useCurrentTime'

interface WeatherContainerProps {
  loading?  : boolean;
  visible   : boolean;
  children? : React.ReactNode;
}

const WeatherContainer: React.FC<WeatherContainerProps> = ({ 
  loading = false, 
  visible = true, 
  children
}) => {
  const [searchParams] = useSearchParams()

  const mode = searchParams.get('mode') ?? 'weather'
  const city = searchParams.get('city')

  const { formattedTime } = useCurrentTime('HH:mm:ss');

  return (
    <Container my={40} pos="relative" miw={500} mih={220}>
      <LoadingOverlay zIndex={1000} visible={loading} overlayProps={{ radius: 'sm', blur: 2 }} />

      <Collapse in={visible} transitionDuration={1000} transitionTimingFunction="linear">
        <Card shadow="sm" padding="lg" radius="md" withBorder bg="#cecece10">
          <Card.Section withBorder inheritPadding py="xs">
            <Group justify="space-between">
              <Text fw={500} variant="gradient" gradient={{ from: 'pink', to: 'yellow' }}>
                {capitalize(mode)} {city ? `for ${city}` : ''}
              </Text>

              <Title order={5} fw={100}>
                {formattedTime}
              </Title>
            </Group>
          </Card.Section>

          {children}
        </Card>
      </Collapse>
    </Container>
  );
};

export default WeatherContainer;
