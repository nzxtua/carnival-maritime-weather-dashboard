import { IconSun, IconMoonStars }                              from '@tabler/icons-react'
import { rem, Switch, useMantineTheme, useMantineColorScheme } from '@mantine/core'

const ThemeSwitcher: React.FC = () => {
  const theme = useMantineTheme()
  const { colorScheme, toggleColorScheme } = useMantineColorScheme()

  const sunIcon = (
    <IconSun
      style  = {{ width: rem(16), height: rem(16) }}
      stroke = {2.5}
      color  = {theme.colors.yellow[4]}
    />
  );

  const moonIcon = (
    <IconMoonStars
      style  = {{ width: rem(16), height: rem(16) }}
      stroke = {2.5}
      color  = {theme.colors.blue[6]}
    />
  );

  return (
    <Switch 
      size         = "md"
      color        = "dark.4"
      onLabel      = {sunIcon}
      offLabel     = {moonIcon}
      checked      = {colorScheme === 'light'}
      onChange     = {toggleColorScheme}
      data-testid  = "switch-input"
      wrapperProps = {{ 'data-testid': 'switch-wrapper' }} 
    />
  )
}

export default ThemeSwitcher