import { MantineProvider }                        from '@mantine/core'
import { useMantineTheme, useMantineColorScheme } from '@mantine/core'
import { render, fireEvent }                      from '@testing-library/react'
import ThemeSwitcher                              from './ThemeSwitcher'

// Mocking useMantineTheme and useMantineColorScheme hooks
vi.mock('@mantine/core', async () => {
  const originalModule = await vi.importActual('@mantine/core');
  return {
    ...originalModule,
    useMantineTheme: vi.fn(),
    useMantineColorScheme: vi.fn(),
  };
});

describe('ThemeSwitcher', () => {
  it('renders correctly with light theme', () => {
    (useMantineTheme as ReturnType<typeof vi.fn>).mockReturnValue({
      colors: {
        yellow: { 4: 'yellow' },
        blue: { 6: 'blue' },
      },
    });
    (useMantineColorScheme as ReturnType<typeof vi.fn>).mockReturnValue({
      colorScheme: 'light',
      toggleColorScheme: vi.fn(),
    });

    const { getByTestId } =render(
      <MantineProvider forceColorScheme='light'>
        <ThemeSwitcher />
      </MantineProvider>
    );

    const switchInput = getByTestId('switch-input');
    expect(switchInput).toBeChecked();
    const switchWrapper = getByTestId('switch-wrapper');
    expect(switchWrapper).toBeInTheDocument();
  });

  it('renders correctly with dark theme', () => {
    (useMantineTheme as ReturnType<typeof vi.fn>).mockReturnValue({
      colors: {
        yellow: { 4: 'yellow' },
        blue: { 6: 'blue' },
      },
    });
    (useMantineColorScheme as ReturnType<typeof vi.fn>).mockReturnValue({
      colorScheme: 'dark',
      toggleColorScheme: vi.fn(),
    });

    const { getByTestId } = render(
      <MantineProvider forceColorScheme='dark'>
        <ThemeSwitcher />
      </MantineProvider>
    );

    const switchInput = getByTestId('switch-input');
    expect(switchInput).not.toBeChecked();
    const switchWrapper = getByTestId('switch-wrapper');
    expect(switchWrapper).toBeInTheDocument();
  });

  it('toggles theme on change', () => {
    const toggleColorSchemeMock = vi.fn();

    (useMantineTheme as ReturnType<typeof vi.fn>).mockReturnValue({
      colors: {
        yellow: { 4: 'yellow' },
        blue: { 6: 'blue' },
      },
    });
    (useMantineColorScheme as ReturnType<typeof vi.fn>).mockReturnValue({
      colorScheme       : 'light',
      toggleColorScheme : toggleColorSchemeMock,
    });

    const { getByTestId } = render(
      <MantineProvider forceColorScheme='light'>
        <ThemeSwitcher />
      </MantineProvider>
    );

    const switchInput = getByTestId('switch-input');
    fireEvent.click(switchInput);
    expect(toggleColorSchemeMock).toHaveBeenCalledTimes(1);
  });
});
