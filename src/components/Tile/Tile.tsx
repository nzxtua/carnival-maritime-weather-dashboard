import { Card, Group, Text, Title } from '@mantine/core'
import { 
	IconWind, 
	IconCloudRain, 
	IconEye, 
	IconTemperature, 
	IconWiper,
	IconRipple,
}                                   from '@tabler/icons-react'
import classes                      from './Tile.module.css'

export interface TileProps {
  icon         : 'wind' | 'feels' | 'humidity' | 'visibility' | 'pressure' | 'pop';
  title        : string;
  info         : string | JSX.Element;
  description? : string | JSX.Element;
}

const icons = {
  wind       : IconWind,
  feels      : IconTemperature,
  humidity   : IconRipple,
  visibility : IconEye,
  pressure   : IconWiper,
  pop        : IconCloudRain, 
}

const Tile: React.FC<TileProps> = ({ icon, title, info, description }) => {
  const Icon = icons[icon]

  return (
    <Card padding='lg' shadow='md' className={classes.tile}>
      <Group>
        <Icon data-testid='icon' /><Title order={5}>{title}</Title>
      </Group>

      <Title order={3}>{info}</Title> 

      <Text c='dimmed' data-testid="description">{description}</Text>
    </Card>
  )
}

export default Tile
