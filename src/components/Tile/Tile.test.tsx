import { render }          from '@test-utils'
import Tile, { TileProps } from './Tile'

describe('Tile', () => {
  const defaultProps: TileProps = {
    icon         : 'wind',
    title        : 'wind title',
    info         : 'wind info',
    description  : 'wind description',
  }

  it('renders without crashing', () => {
    const { container } = render(<Tile {...defaultProps} />)
    expect(container).toBeInTheDocument()
  })

  it('renders icon based on prop', () => {
    const { getByTestId } = render(<Tile {...defaultProps} icon='wind' />)
    expect(getByTestId('icon')).toBeInTheDocument()
  })

  it('renders title based on prop', () => {
    const { getByText } = render(<Tile {...defaultProps} title='test title' />)
    expect(getByText('test title')).toBeInTheDocument()
  })

  it('renders info based on prop', () => {
    const { getByText } = render(<Tile {...defaultProps} info='test info' />)
    expect(getByText('test info')).toBeInTheDocument()
  })

  it('renders description based on prop', () => {
    const { getByText } = render(<Tile {...defaultProps} description='test description' />)
    expect(getByText('test description')).toBeInTheDocument()
  })

  it('renders description even if it is not provided', () => {
    const { getByTestId } = render(<Tile {...defaultProps} description={undefined} />)
    expect(getByTestId('description')).toBeEmptyDOMElement()
  })
})

export default Tile

