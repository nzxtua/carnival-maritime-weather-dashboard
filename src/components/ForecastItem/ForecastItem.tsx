import dayjs                     from 'dayjs'
import { 
	Card, 
	Stack, 
	Title, 
	Image, 
}                                from '@mantine/core'
import { Degree }                from '@/components'
import type { ForecastListItem } from '@/types/forecast'
import classes                   from './ForecastItem.module.css'

interface ForecastItemProps extends ForecastListItem {}

const OPENWEATHER_API_IMAGE_URL = 'https://openweathermap.org/img/wn'

const ForecastItem: React.FC<ForecastItemProps> = ({ dt_txt, main, weather }) => {
	return (
		<Card shadow="sm" padding="md">
			<Stack align='center'>
				<Title order={4} fw={300} textWrap='nowrap'>
					{dayjs(dt_txt).format('DD MMM HH:mm')}
				</Title>
				
				<Image
					alt = {`weather-icon-${weather?.[0].description}`}
					src = {`${OPENWEATHER_API_IMAGE_URL}/${weather?.[0].icon}@2x.png`}
				/>

				<Degree value={main?.temp} className={classes.temperature} data-testid="degree">
					Avg. Temp:
				</Degree>
			</Stack>
		</Card>
	)
}

export default ForecastItem