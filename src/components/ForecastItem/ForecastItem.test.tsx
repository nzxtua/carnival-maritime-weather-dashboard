import { render }				         from '@test-utils'
import type { ForecastListItem } from '@/types/forecast'
import ForecastItem              from './ForecastItem'
import classes                   from './ForecastItem.module.css'

describe('ForecastItem', () => {
	const item = {
		dt_txt  : '2021-12-12 12:00:00',
		main    : { temp: 12.3 },
		weather : [{ description: 'cloudy', icon: '04d' }],
	} as ForecastListItem

	it('renders the date and time', () => {
		const { getByText } = render(<ForecastItem {...item} />)
		expect(getByText('12 Dec 12:00')).toBeInTheDocument()
	})

	it('renders the weather icon', () => {
		const { getByAltText } = render(<ForecastItem {...item} />)
		expect(getByAltText('weather-icon-cloudy')).toBeInTheDocument()
	})

	it('renders the average temperature', () => {
		const { getByText, getByTestId } = render(<ForecastItem {...item} />)
		expect(getByText('Avg. Temp: 12')).toBeInTheDocument()
		expect(getByTestId('degree')).toHaveClass(classes.temperature)
	})

	it('handles missing data gracefully', () => {
		const { getByText, queryByAltText, queryByText } = render(<ForecastItem {...{}} />)
		expect(getByText('Avg. Temp: —')).toBeInTheDocument()
		expect(queryByAltText('weather-icon-')).not.toBeInTheDocument()
		expect(queryByText('Avg. Temp:')).not.toBeInTheDocument()
	})
})
