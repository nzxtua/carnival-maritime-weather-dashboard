import { Title, Text } from '@mantine/core'
import classes         from './Welcome.module.css'

const Welcome = () => {
  return (
    <>
      <Title className={classes.title} ta="center" mt={80}>
        Welcome to{' '}
        <br />
        <Text inherit variant="gradient" component="span" gradient={{ from: 'pink', to: 'yellow' }}>
          Carnival Maritime - Weather App
        </Text>
      </Title>
    </>
  )
}

export default Welcome