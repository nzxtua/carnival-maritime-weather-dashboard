import { render } from '@test-utils'
import Welcome    from './Welcome'

describe('Welcome component', () => {
  it('has correct Vite guide link', () => {
    const { getByText } = render(<Welcome />);
    expect(getByText('Carnival Maritime - Weather App')).toBeInTheDocument()
  })
});
