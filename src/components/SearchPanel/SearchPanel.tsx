import { useState }                  from 'react'
import { useSearchParams }           from 'react-router-dom'
import { useDebouncedValue }         from '@mantine/hooks'
import { IconX, IconMapPin }         from '@tabler/icons-react'
import { 
  Autocomplete, 
  Group, 
  Container, 
  Title, 
  Image,
  Card, 
  ComboboxStringData,
  rem,
  Tooltip
}                                    from '@mantine/core'
import { ThemeSwitcher }             from '@/components'
import type { CityResponse }         from '@/types/city'
import type { Coords, Mode, Option } from '@/types/extra'
import { useFetchCities }            from '@/hooks/useFetchCities'
import classes                       from './SearchPanel.module.css'

interface SearchPanelProps {}

const SearchPanel: React.FC<SearchPanelProps> = () => {
  const [searchParams, setSearchParams] = useSearchParams()

  const mode = searchParams.get('mode') as Mode || 'weather'

	const [inputValue, setInputValue] = useState(searchParams.get('q') ?? '');
	const [searchQuery] = useDebouncedValue(inputValue, 1000);

  const handleCoordinatesChange = (coords: Coords) => {
    if (coords) {
      const option = cities!.find(item => item.value === `${coords[0]} ${coords[1]}`)

      option && searchParams.set('city', option?.label)

      searchParams.set('lat', coords[0])
      searchParams.set('lon', coords[1])
    } else {
      searchParams.delete('city')
      searchParams.delete('lat')
      searchParams.delete('lon')
    }

    setSearchParams(searchParams) 
  } 

	const cleanInput = () => {
		setInputValue('');
		handleCoordinatesChange(null)
	}

	const onOptionSubmit = (value: string) => {
		const [lat, lon] = value.split(" ")
		handleCoordinatesChange([lat, lon])
	};
	
  const { data: cities, isFetching } = useFetchCities<CityResponse[], Option[]>(searchQuery, {
    select: data =>
      data.map(item => ({
        value : `${item.lat} ${item.lon}`,
        label : `${item.name}${item.state ? `, ${item.state}` : ''}, ${item.country}`,
      })),
  });

  const onModeChange = (value: Mode) => {
    searchParams.set('mode', value)
    setSearchParams(searchParams)
  }

	const pinIcon    = <IconMapPin style={{ width: rem(24), height: rem(24) }} />
	const loaderIcon = (
  <IconX 
    style     = {{ width: rem(24), height: rem(24) }}
    className = {isFetching ? classes.spinningIcon : classes.cursor}
    onClick   = {cleanInput}
    />
  )
 
	return (
    <Container my={80} maw={600}>
      <Card shadow="sm" padding="lg" radius="md" withBorder>
        <Card.Section withBorder inheritPadding py="xs">
          <Group justify="space-between">
            <Tooltip arrowOffset={10} arrowSize={8} offset={10} label="Click the title to choose mode" withArrow opened position="top-end" color="blue">
              <Title className={classes.cursor}>
                <span 
                  className = {mode === 'forecast' ? classes.lightText : ''}
                  onClick   = {() => onModeChange('weather')}
                  >
                    Weather
                  </span>
                {' '}
                <span 
                  className = {mode === 'weather' ? classes.lightText : ''}
                  onClick   = {() => onModeChange('forecast')}
                >
                  Forecast
                </span>
              </Title>
            </Tooltip>

            <ThemeSwitcher />
          </Group>
        </Card.Section>

        <Card.Section>
          <Image
            src    = "https://raw.githubusercontent.com/mantinedev/mantine/master/.demo/images/bg-8.png"
            alt    = "Norway"
            height = {100}
          />
        </Card.Section>

        <Card.Section withBorder inheritPadding py="xs">
          <Tooltip 
            opened 
            withArrow
            color       = "blue"
            offset      = {10}
            arrowSize   = {8}
            arrowOffset = {10}
            position    = "bottom-end"
            label       = "Enter a city to get weather information"
          >
            <Autocomplete
							my             = {10}
							size           = 'xl'
							radius         = 'md'
							placeholder    = "City, State, Country"
							comboboxProps  = {{ shadow: 'md' }}
							leftSection    = {pinIcon}
							rightSection   = {!!inputValue ? loaderIcon : null}
							data           = {cities as ComboboxStringData}
							value          = {inputValue}
							onChange       = {setInputValue}
							onOptionSubmit = {onOptionSubmit}
            />
          </Tooltip>
        </Card.Section>
      </Card>
    </Container>
  );
};

export default SearchPanel
