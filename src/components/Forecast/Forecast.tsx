import { useMemo }                        from 'react'
import { useSearchParams }                from 'react-router-dom'
import { Carousel }                       from '@mantine/carousel'
import { Card }                           from '@mantine/core'
import type { Coords }                    from '@/types/extra'
import { WeatherContainer, ForecastItem } from '@/components'
import { useFetchWeatherForecast }        from '@/hooks/useFetchWeatherForecast'
import classes                            from './Forecast.module.css'
import type { ForecastResponse }          from '@/types/forecast'

interface ForecastProps {}

const Forecast: React.FC<ForecastProps> = ({}) => {
  const [searchParams] = useSearchParams()
	
  const coordinates = useMemo<Coords>(() => {
    const lat = searchParams.get('lat');
    const lon = searchParams.get('lon');

    if (lat && lon) {
      return [lat, lon];
    }

    return null;
  }, [searchParams]);

  const {
    data: forecast,
    isLoading,
    isSuccess,
  } = useFetchWeatherForecast<ForecastResponse>(coordinates)

  return (
    <WeatherContainer loading={isLoading} visible={!!coordinates}>
      {isSuccess && (
        <Card.Section>
          <Carousel
            loop
            withIndicators
            slideSize  = {{ base: '33.333%', sm: '25%', md: '10%' }}
            slideGap   = {{ base: 0, sm: 'md' }}
            align      = "start"
            classNames = {classes}
          >
            {forecast?.list?.map((item) => (
              <Carousel.Slide key={item.dt} my='lg'>
                <ForecastItem {...item} />
              </Carousel.Slide>
            ))}
          </Carousel>
        </Card.Section>
      )}
    </WeatherContainer>
  )
}

export default Forecast
