import { useMemo }                from 'react'
import { useSearchParams }        from 'react-router-dom'
import {
  Box,
  Card,
  Text,
  Image,
  Group,
  Stack,
  Flex,
}                                 from '@mantine/core'
import { WeatherResponse }        from '@/types/weather'
import { useFetchCurrentWeather } from '@/hooks/useFetchCurrentWeather'
import { 
  Tile, 
  SimpleTile, 
  Degree, 
  WeatherContainer
}                                 from '@/components'
import {
  getHumidityValue,
  getPop,
  getSunTime,
  getVisibilityValue,
  getWindDirection,
}                                 from '@/utils/helpers'
import type { Coords }            from '@/types/extra'
import classes                    from './CurrentWeather.module.css'

interface CurrentWeatherProps {}

const OPENWEATHER_API_IMAGE_URL = 'https://openweathermap.org/img/wn'

const CurrentWeather: React.FC<CurrentWeatherProps> = () => {
  const [searchParams] = useSearchParams()
	
  const coordinates = useMemo<Coords>(() => {
    const lat = searchParams.get('lat');
    const lon = searchParams.get('lon');

    if (lat && lon) {
      return [lat, lon];
    }

    return null;
  }, [searchParams]);

  const {
    data: today,
    isLoading,
    isSuccess,
  } = useFetchCurrentWeather<WeatherResponse>(coordinates)

  return (
    <WeatherContainer loading={isLoading} visible={!!coordinates}>
      {isSuccess && (
        <Card.Section>
          <Box p="lg">
            <Flex justify="space-between" align="center">
              <Image
                alt={`weather-icon-${today!.weather[0].description}`}
                src={`${OPENWEATHER_API_IMAGE_URL}/${today!.weather[0].icon}@4x.png`}
              />

              <Stack gap={0}>
                <Degree className={classes.temperature} value={today!.main.temp} />
                <Degree className={classes.feelsLike} value={today!.main.feels_like}>
                  {`Feels ${Math.round(today!.main.feels_like) < today!.main.temp ? 'Cooler' : 'Hotter'}:`}
                </Degree>

                <Group>
                  <Degree className={classes.minmax} value={today!.main.temp_max}>
                    {'H:'}
                  </Degree>
                  <Degree className={classes.minmax} value={today!.main.temp_min}>
                    {'L:'}
                  </Degree>
                </Group>
              </Stack>

              <Stack gap={0} className={classes.weatherInfo}>
                <Text className={classes.weather}>{today!.weather[0].main}</Text>
                <Text className={classes.description}>{today!.weather[0].description}</Text>
              </Stack>
            </Flex>

            <Flex 
              gap  = {10}
              wrap = "wrap"
            >
              <Tile
                icon        = "wind"
                title       = "Wind"
                info        = {`${Math.round(today!.wind.speed)} km/h`}
                description = {`${getWindDirection(Math.round(today!.wind.deg))}, gusts ${today!.wind.gust?.toFixed()} km/h`}
              />

              <Tile
                icon        = "humidity"
                title       = "Humidity"
                info        = {`${today!.main.humidity} %`}
                description = {getHumidityValue(today!.main.humidity)}
              />

              <Tile
                icon        = "pressure"
                title       = "Pressure"
                info        = {`${today!.main.pressure} hPa`}
                description = {`${Math.round(today!.main.pressure) < 1013 ? 'Lower' : 'Higher'} than standard`}
              />

              <Tile
                icon        = "pop"
                title       = "Precipitation"
                info        = {`${Math.round(today!.rain?.['1h']) || 0} %`}
                description = {`${getPop(today!.rain?.['1h'])}, clouds at ${today?.clouds.all}%`}
              />

              <Tile
                icon        = "visibility"
                title       = "Visibility"
                info        = {`${(today!.visibility / 1000)?.toFixed()} km`}
                description = {getVisibilityValue(today!.visibility)}
              />

              <SimpleTile icon="sunrise" info={getSunTime(today!.sys.sunrise)} />
              <SimpleTile icon="sunset" info={getSunTime(today!.sys.sunset)} />
            </Flex>
          </Box>
        </Card.Section>
      )}
    </WeatherContainer>
  );
};

export default CurrentWeather;
