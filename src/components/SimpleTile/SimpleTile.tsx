import { Card, Stack, Title }      from '@mantine/core'
import { IconSunrise, IconSunset } from '@tabler/icons-react'
import classes                     from './SimpleTile.module.css'


export interface SimpleTileProps {
  icon : 'sunrise' | 'sunset';
  info : string | JSX.Element;
}

const icons = {
  sunrise : IconSunrise,
  sunset  : IconSunset,
}

const SimpleTile: React.FC<SimpleTileProps> = ({ icon, info }) => {
  const Icon = icons[icon]

  return (
    <Card padding='lg' shadow='md' className={classes.simpleTile}>
      <Stack align='center' justify='center'>
        <Icon height={36} width={36} data-testid="icon" data-icon={icon} />
	      <Title order={3}>{info}</Title> 
      </Stack>
    </Card>
  )
}
export default SimpleTile
