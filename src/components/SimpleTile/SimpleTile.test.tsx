import { render }                      from '@test-utils'
import SimpleTile, { SimpleTileProps } from './SimpleTile'

const defaultProps: SimpleTileProps = {
  icon: 'sunrise',
  info: '10:00 AM',
};

describe('SimpleTile', () => {
  it('renders without crashing', () => {
    render(<SimpleTile {...defaultProps} />);
  })

  it('renders correct icon', () => {
    const { getByTestId } = render(<SimpleTile {...defaultProps} />);
    expect(getByTestId('icon').getAttribute('data-icon')).toBe('sunrise');
  })

  it('renders correct info', () => {
    const { getByText } = render(<SimpleTile {...defaultProps} />);
    expect(getByText('10:00 AM')).toBeInTheDocument();
  })
})

