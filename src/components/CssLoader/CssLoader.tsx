import { forwardRef }                  from 'react'
import cx                              from 'clsx'
import { Box, MantineLoaderComponent } from '@mantine/core'
import classes                         from './CssLoader.module.css'

const CssLoader: MantineLoaderComponent = forwardRef(({
	className,
	...others
}, ref) => (
  <Box 
		ref       = {ref}
		component = "span"
		className = {cx(classes.loader, className)}
		{...others}
	/>
))

export default CssLoader