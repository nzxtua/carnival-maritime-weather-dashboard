import { render } from '@test-utils'
import Degree     from './Degree'

describe('Degree', () => {
	it('renders with default props', () => {
		const { getByText } = render(<Degree />)
		expect(getByText('—')).toBeInTheDocument()
	})
	it('renders with provided props', () => {
		const { getByText } = render(<Degree value={23} className='myClass'>Temperature: </Degree>)
		const element = getByText('Temperature: 23')
		expect(element).toBeInTheDocument()
		expect(element).toHaveClass('myClass')
	})
})
