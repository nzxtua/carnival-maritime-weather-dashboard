import cx       from 'clsx'
import { Text } from '@mantine/core'
import classes  from './Degree.module.css'

interface DegreeProps {
	value?     : number | null;
	children?  : React.ReactNode;
	className? : string | null;
}

const Degree: React.FC<DegreeProps> = ({ value, children, className, ...props }) => {
	return (
		<Text className={cx(classes.celsius, className)} {...props}>
			{children} {value?.toFixed() ?? '—'}
		</Text>
	)
}

export default Degree