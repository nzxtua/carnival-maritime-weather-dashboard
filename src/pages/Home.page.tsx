import React               from 'react'
import { Center, Loader }  from '@mantine/core'
import { useSearchParams } from 'react-router-dom'

const Welcome        = React.lazy(() => import('@/components/Welcome/Welcome'))
const Forecast       = React.lazy(() => import('@/components/Forecast/Forecast'))
const SearchPanel    = React.lazy(() => import('@/components/SearchPanel/SearchPanel'))
const CurrentWeather = React.lazy(() => import('@/components/CurrentWeather/CurrentWeather'))

export const HomePage = () => {
  const [searchParams] = useSearchParams();
	
	const mode = searchParams.get('mode');

  return (
    <React.Suspense fallback={<Center h="100vh"><Loader /></Center>}>
      <Welcome />

      <SearchPanel />
      
      {mode === 'forecast' 
        ? <Forecast />
        : <CurrentWeather />
      }
    </React.Suspense>
  );
}

