type Locale = 'en' | 'hr' | 'hu' | 'pl' | 'uk' | 'ru' | 'ko' | 'de';

type LocalNames = Record<Locale, string>;

export interface CityResponse {
  name: string;
  local_names: LocalNames;
  lat: number;
  lon: number;
  country: string;
  state: string;
}
