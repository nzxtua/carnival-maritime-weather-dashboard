export type Coords = [string, string] | null
export type Option = { value: string, label: string }
export type Mode = 'weather' | 'forecast'
