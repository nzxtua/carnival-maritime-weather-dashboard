import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { MantineProvider } from '@mantine/core';
import { theme } from './theme';

const queryClient = new QueryClient()

export const Providers: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  return (
    <MantineProvider theme={theme}>
      <QueryClientProvider client={queryClient}>
        {children}
      </QueryClientProvider>
    </MantineProvider>
  );
};
