import axios from 'axios';

export const apiClient = axios.create({
  baseURL: import.meta.env.VITE_OPENWEATHER_API_BASEURL,
});

