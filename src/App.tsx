import '@mantine/core/styles.css'
import '@mantine/carousel/styles.css'
import { Router }    from '@/Router'
import { Providers } from '@/Providers'

export default function App() {
  return (
    <Providers>
      <Router />
    </Providers>
  );
}
